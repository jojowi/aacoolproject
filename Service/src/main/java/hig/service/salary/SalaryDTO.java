package hig.service.salary;

import hig.service.person.PersonDTO;

import java.time.LocalDate;

public record SalaryDTO(
        PersonDTO person,
        LocalDate effectiveDate,
        Long salaryAmount) {
}

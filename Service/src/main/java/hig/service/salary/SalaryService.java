package hig.service.salary;

import hig.domain.Salary;
import hig.domain.Person;
import hig.repository.SalaryRepository;
import hig.repository.PersonRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;

@Service
public class SalaryService {
    private final SalaryRepository salaryRepository;
    private final SalaryMapper mapper;
    private final PersonRepository personRepository;

    public SalaryService(SalaryRepository salaryRepository, SalaryMapper mapper, PersonRepository personRepository) {
        this.salaryRepository = salaryRepository;
        this.mapper = mapper;
        this.personRepository = personRepository;
    }

    public List<SalaryDTO> getAllSalaries() {
        return mapper.toDtoList(salaryRepository.findAll());
    }

    public SalaryDTO create(String targetUUID, LocalDate effectiveDate, Long salaryAmount) {
        Person person = personRepository.findByTag(targetUUID).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));

        if (salaryRepository.countByPersonId(person.getId()) > 0 && effectiveDate.isBefore(salaryRepository
                .getLatestSalaryByPersonID(person.getId())
                .orElseThrow(() -> new EntityNotFoundException("No salary found :("))
                .getEffectiveDate())) {
            throw new DateTimeException("New salary cannot be before existing salary");
        }

        return mapper.toDTO(salaryRepository.save(new Salary(effectiveDate, person, salaryAmount)));
    }

    public void update(String targetUUID, Long salaryAmount) {
        Person person = personRepository.findByTag(targetUUID).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));
        Salary latestSalary = salaryRepository.getLatestSalaryByPersonID(person.getId()).orElseThrow(() -> new EntityNotFoundException("No salary found :("));

        if (latestSalary.isUsed()) {
            throw new SecurityException("Cannot change already used salary");
        }

        salaryRepository.updateSalarySetLatestAmountById(latestSalary.getId(), salaryAmount);
    }
}

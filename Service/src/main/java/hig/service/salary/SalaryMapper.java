package hig.service.salary;

import hig.domain.Salary;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface SalaryMapper {

    SalaryDTO toDTO(Salary salary);

    List<SalaryDTO> toDtoList(List<Salary> salaries);
}

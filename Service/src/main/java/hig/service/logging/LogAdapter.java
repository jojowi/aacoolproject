package hig.service.logging;

import java.util.function.Supplier;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Component
public interface LogAdapter {

    void log(Level level, Supplier<String> messageSupplier);
}

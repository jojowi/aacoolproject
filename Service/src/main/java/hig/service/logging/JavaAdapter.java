package hig.service.logging;

import java.util.Map;
import java.util.function.*;
import java.util.logging.Logger;

/**
 *
 * @author thomas
 */
public class JavaAdapter implements LogAdapter {

    private static final Logger logger = Logger.getLogger("JAVA");
    private final Map<Level, Consumer<String>> methodMap
            = Map.of(Level.INFO, logger::info,
                    Level.DEBUG, logger::finest,
                    Level.WARNING, logger::warning,
                    Level.ERROR, logger::severe);

    @Override
    public void log(Level level, Supplier<String> messageSupplier) {
        methodMap.get(level).accept(messageSupplier.get());
    }
}

package hig.service.logging;

/**
 *
 * @author thomas
 */
public enum Level {
    INFO, DEBUG, WARNING, ERROR;
}

package hig.service.person;

/**
 *
 * @author thomas
 */
public record PersonDTO(String firstName, String lastName) {
}

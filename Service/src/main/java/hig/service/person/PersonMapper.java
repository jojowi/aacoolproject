package hig.service.person;

import hig.domain.Person;
import java.util.List;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Component
@Mapper(componentModel = "spring")
public interface PersonMapper {

    @Mapping(target = "name", expression = "java(person.getFirstName() + \" \" + person.getLastName())")
    PersonSimpleDTO toSimpleDto(Person person);

    PersonDTO toDto(Person person);

    List<PersonSimpleDTO> toSimpleDtoList(List<Person> person);

    List<PersonDTO> toDtoList(List<Person> person);
}

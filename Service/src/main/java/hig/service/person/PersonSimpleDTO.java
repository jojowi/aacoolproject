package hig.service.person;

/**
 *
 * @author thomas
 */
public record PersonSimpleDTO(String name, Integer birthDate, Boolean admin) {
}

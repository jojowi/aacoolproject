package hig.service.room;

/**
 *
 * @author thomas
 */
public record RoomDTO(Long id, String name) {
}

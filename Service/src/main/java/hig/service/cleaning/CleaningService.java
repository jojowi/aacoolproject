package hig.service.cleaning;

import hig.domain.*;
import hig.repository.*;
import jakarta.persistence.EntityNotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class CleaningService {

    private final CleaningRepository repository;
    private final PersonRepository personRepository;
    private final RoomRepository roomRepository;
    private final CleaningMapper mapper;

    @Autowired
    public CleaningService(CleaningRepository repository, PersonRepository personRepository, RoomRepository roomRepository, CleaningMapper mapper) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.roomRepository = roomRepository;
        this.mapper = mapper;
    }

    public List<CleaningDTO> getAllCleanings() {
        return mapper.toDtoList(repository.findAll());
    }

    public List<CleaningDTO> getCleaningsByRoomIdAndDate(Long roomId, LocalDateTime fromDate) {
        return mapper.toDtoList(repository.findCleaningsByRoomIdAndDateTimeStampAfter(roomId, fromDate));
    }

    public CleaningDTO create(Long roomId, String personalKey) {
        Person p = personRepository.findByTag(personalKey).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));
        Room r = roomRepository.findById(roomId).orElseThrow(() -> new EntityNotFoundException("No room with given id found"));
        return mapper.toDto(repository.save(new Cleaning(p, r)));
    }
    
    public List<CleaningDTO> getByPersonIdAndDaysForward(String personTag, Long days){
        Long personId = personRepository.findByTag(personTag).get().getId();
        return mapper.toDtoList(repository.findByPersonIdAndDateTimeStampGreaterThan(personId, LocalDateTime.now().minusDays(days)));    
    }
     public List<CleaningDTO> getByPersonIdAndDaysBackward(String personTag, Long days){   
        Long personId = personRepository.findByTag(personTag).get().getId();
        return mapper.toDtoList(repository.findByPersonIdAndDateTimeStampLessThan(personId, LocalDateTime.now().minusDays(days)));  
     }


    public List<CleaningDTO> getCleaningsByUserTag(String targetUUID, LocalDateTime fromDate, LocalDateTime toDate) {
        Person p = personRepository.findByTag(targetUUID).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));

        return mapper.toDtoList(repository.findCleaningsByPersonIdAndDateTimeStampBetween(p.getId(), fromDate, toDate));
    }
}

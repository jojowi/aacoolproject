package hig.service.cleaning;

import com.fasterxml.jackson.annotation.JsonFormat;
import hig.service.person.PersonDTO;
import hig.service.person.PersonSimpleDTO;
import hig.service.room.RoomDTO;

import java.time.LocalDateTime;

/**
 * @author thomas
 */
public record CleaningDTO(
        RoomDTO room,
        PersonDTO person,
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime dateTimeStamp) {
}

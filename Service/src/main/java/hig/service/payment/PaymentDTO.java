package hig.service.payment;

import hig.domain.Person;

import java.time.LocalDate;

public record PaymentDTO(LocalDate paymentDate, Person person, Long paymentAmount) {
}

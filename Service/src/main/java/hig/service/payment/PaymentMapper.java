package hig.service.payment;

import hig.domain.Payment;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface PaymentMapper {

    PaymentDTO toDTO(Payment payment);

    List<PaymentDTO> toDTO(List<Payment> payments);
}

package hig.service.payment;

import hig.domain.Payment;
import hig.domain.Person;
import hig.domain.Salary;
import hig.repository.CleaningRepository;
import hig.repository.PaymentRepository;
import hig.repository.PersonRepository;
import hig.repository.SalaryRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;

@Service
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private final PaymentMapper mapper;
    private final PersonRepository personRepository;
    private final SalaryRepository salaryRepository;
    private final CleaningRepository cleaningRepository;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository,
                          PaymentMapper mapper,
                          PersonRepository personRepository,
                          SalaryRepository salaryRepository,
                          CleaningRepository cleaningRepository) {
        this.paymentRepository = paymentRepository;
        this.mapper = mapper;
        this.personRepository = personRepository;
        this.salaryRepository = salaryRepository;
        this.cleaningRepository = cleaningRepository;
    }

    public PaymentDTO create(String targetUUID, LocalDate paymentDate) {


        if (LocalDate.now().isBefore(paymentDate)) {
            throw new DateTimeException("Payment date can't be in the future");
        }

        Person person = personRepository.findByTag(targetUUID).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));
        Salary salary = salaryRepository.getLatestSalaryByPersonID(person.getId()).orElseThrow(() -> new EntityNotFoundException("No salary found for specified user"));
        LocalDate previousPaymentDate = paymentRepository.getLatestPaymentDateByPersonID(person.getId()).orElse(LocalDate.of(0,1,1));

        if (paymentDate.isBefore(previousPaymentDate)) {
            throw new DateTimeException("New payment cannot be before existing payment");
        }

        long amountOfCleanings = cleaningRepository.findCleaningsByPersonIdAndDateTimeStampBetween(person.getId(), previousPaymentDate.atStartOfDay(), paymentDate.atStartOfDay()).size();
        long amountPerCleaning = salary.getSalaryAmount();

        long paymentAmount = amountOfCleanings * amountPerCleaning;

        salaryRepository.updateSalarySetUsedById(salary.getId());

        return mapper.toDTO(paymentRepository.save(new Payment(paymentDate, person, paymentAmount)));

    }

    public List<PaymentDTO> findAll() {
        return mapper.toDTO(paymentRepository.findAll());
    }
}

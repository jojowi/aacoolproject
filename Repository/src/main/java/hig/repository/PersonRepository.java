package hig.repository;

import hig.domain.Person;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    public Optional<Person> findByTag(String personalTag);
    
    public Integer deleteByTag(String personalTag);
    
}

package hig.repository;

import hig.domain.Cleaning;
import java.time.*;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface CleaningRepository extends JpaRepository<Cleaning, Long>{

//    public List<Cleaning> findByRoomAndWhenDateTimeStampGreaterThan(Long roomId, LocalDateTime from);
    public List<Cleaning> findByPersonIdAndDateTimeStampGreaterThan(Long personId, LocalDateTime dateTimeStamp);
    public List<Cleaning> findByPersonIdAndDateTimeStampLessThan(Long personId, LocalDateTime dateTimeStamp);
    public List<Cleaning> findCleaningsByRoomIdAndDateTimeStampAfter(Long roomId, LocalDateTime dateTimeStamp);
    public List<Cleaning> findCleaningsByPersonIdAndDateTimeStampBetween(Long personId, LocalDateTime start, LocalDateTime end);

    public Long countByPersonId(Long personId);




}



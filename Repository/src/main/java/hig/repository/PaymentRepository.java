package hig.repository;

import hig.domain.Payment;
import hig.domain.Salary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;


@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
//    @Query("SELECT s.paymentDate FROM Payment s WHERE s.id = (SELECT MAX(s2.id) FROM Payment s2) AND s.person.id = :person_Id")
    @Query("SELECT s.paymentDate FROM Payment s WHERE s.id = (SELECT MAX(s2.id) FROM Payment s2 WHERE s2.person.id = :person_Id) AND s.person.id = :person_Id")
    Optional<LocalDate> getLatestPaymentDateByPersonID(@Param("person_Id") Long person_Id);
}

package hig.repository;

import hig.domain.Salary;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SalaryRepository extends JpaRepository<Salary, Long> {
    @Query("SELECT s FROM Salary s WHERE s.id = (SELECT MAX(s2.id) FROM Salary s2 WHERE s2.person.id = :personId)")
    Optional<Salary> getLatestSalaryByPersonID(@Param("personId") Long personId);

    @Transactional
    @Modifying
    @Query("UPDATE Salary s SET s.used = true WHERE s.id = :salaryId")
    void updateSalarySetUsedById(@Param("salaryId") Long salaryId);

    @Transactional
    @Modifying
    @Query("UPDATE Salary s SET s.salaryAmount = :salaryAmount WHERE s.id = :salaryId")
    void updateSalarySetLatestAmountById(@Param("salaryId") Long salaryId, @Param("salaryAmount") Long salaryAmount);

    Long countByPersonId(Long personId);
}

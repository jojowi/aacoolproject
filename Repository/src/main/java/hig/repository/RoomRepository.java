package hig.repository;

import hig.domain.Room;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {


    @Modifying
    @Transactional
    @Query("DELETE FROM Room r WHERE r.id = :roomID")
    public Integer deleteById(@Param("roomID") String roomID);

}

package hig.rest;


import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import hig.domain.Salary;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import hig.repository.SalaryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "69420")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class SalaryControllerIT {

    private Person adminPerson;
    private Person userPerson;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private SalaryRepository salaryRepository;

    @AfterEach
    void tearDown() {
        adminPerson = null;
        userPerson = null;
    }

    @BeforeEach
    void setUp() {
        adminPerson = new Person("Ad", "Min", 1987, true);
        userPerson = new Person("Non", "Min", 1987);
        adminPerson = personRepository.save(adminPerson);
        userPerson = personRepository.save(userPerson);
    }

    @Test
    void withCorrectParameters_thenIsOk() {
        webTestClient.post()
                .uri("/salary")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", userPerson.getTag())
                .header("Effective-Date", LocalDate.now().toString())
                .header("Salary-Amount", "10")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void withIncorrectSalaryAmount_thenIsForbidden() {
        webTestClient.post()
                .uri("/salary")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", userPerson.getTag())
                .header("Effective-Date", LocalDate.now().toString())
                .header("Salary-Amount", "0")
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void cannotCreateSalaryBeforeExistingSalary(){
        salaryRepository.save(new Salary(LocalDate.now(), userPerson, 69420L));

        webTestClient.post()
                .uri("/salary")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", userPerson.getTag())
                .header("Effective-Date", LocalDate.now().minusDays(1).toString())
                .header("Salary-Amount", "1337")
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void canCreateSalaryAfterExistingSalary(){
        salaryRepository.save(new Salary(LocalDate.now(), userPerson, 69420L));

        webTestClient.post()
                .uri("/salary")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", userPerson.getTag())
                .header("Effective-Date", LocalDate.now().plusDays(1).toString())
                .header("Salary-Amount", "1337")
                .exchange()
                .expectStatus().isOk();
    }
}

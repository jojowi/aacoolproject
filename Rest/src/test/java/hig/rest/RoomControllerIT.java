package hig.rest;

import hig.domain.Person;
import hig.domain.Room;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import hig.service.room.RoomService;
import org.h2.tools.Server;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "69420")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class RoomControllerIT {

    private Person adminPerson;
    private Person userPerson;
    private Room roomUnderTest;
    private Room roomUnderTest2;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private RoomService roomService;
    @Autowired
    private RoomRepository roomRepository;

    @BeforeAll
    public static void initTest() throws SQLException {
        Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082")
                .start();
    }

    @BeforeEach
    void setUp() {
        adminPerson = new Person("Ad", "Min", 1987, true);
        userPerson = new Person("Non", "Min", 1987);
        roomUnderTest = new Room( "Vardagsrum");
        roomUnderTest2 = new Room("Toalett");
    }

    @AfterEach
    void tearDown() {
        adminPerson = null;
        userPerson = null;
        roomUnderTest = null;
        roomUnderTest2 = null;
    }

    @Test
    public void withAdminUUID_thenCreationOfRoomWorks() {
        personRepository.save(adminPerson);

        webTestClient.post()
                .uri("/room")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(roomUnderTest)
                .exchange()
                .expectStatus().isOk();


    }

    @Test
    public void withUserUUID_thenCreationOfRoomFails() {
        personRepository.save(userPerson);

        webTestClient.post()
                .uri("/room")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(roomUnderTest)
                .exchange()
                .expectStatus().isForbidden();


    }

    @Test
    public void withAdminUUID_thenDeletionOfRoomWorks() {
        personRepository.save(adminPerson);
        roomUnderTest = roomRepository.save(roomUnderTest);
        roomUnderTest2 = roomRepository.save(roomUnderTest2);

        long expectedCount = 1;

        webTestClient.delete()
                .uri("/room")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Room-ID", roomUnderTest.getId().toString())
                .exchange()
                .expectStatus().isOk();
        assertEquals(expectedCount, roomRepository.count());


    }

    @Test
    public void withUserUUID_thenDeletionOfRoomFails() {
        personRepository.save(userPerson);
        roomUnderTest = roomRepository.save(roomUnderTest);

        long expectedCount = 1;

        webTestClient.delete()
                .uri("/room")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .header("Room-ID", roomUnderTest.getId().toString())
                .exchange()
                .expectStatus().isForbidden();

        assertEquals(expectedCount, roomRepository.count());
    }

    @Test
    public void withNoUUID_thenDeletionOfRoomFails() {
        personRepository.save(adminPerson);
        roomUnderTest = roomRepository.save(roomUnderTest);

        long expectedCount = 1;

        webTestClient.delete()
                .uri("/room")
                .header("secret", "hemlig123hemlig")
                .header("Room-ID", roomUnderTest.getId().toString())
                .exchange()
                .expectStatus().isForbidden();

        assertEquals(expectedCount, roomRepository.count());
    }

    @Test
    public void withNoRoomId_thenDeletionOfRoomFails() {
        personRepository.save(adminPerson);
        roomUnderTest = roomRepository.save(roomUnderTest);

        long expectedCount = 1;

        webTestClient.delete()
                .uri("/room")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .exchange()
                .expectStatus().isBadRequest();

        assertEquals(expectedCount, roomRepository.count());
    }
}
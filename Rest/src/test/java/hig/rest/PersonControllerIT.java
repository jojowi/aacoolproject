package hig.rest;

import hig.domain.Person;
import hig.repository.PersonRepository;
import hig.service.person.PersonService;
import org.h2.tools.Server;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.sql.SQLException;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "69420")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class PersonControllerIT {

    private Person adminPerson;
    private Person userPerson;
    private Person secondUserPerson;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private PersonService personService;

    @BeforeAll
    public static void initTest() throws SQLException {
        Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8069")
                .start();
    }

    @BeforeEach
    void setUp() {
        adminPerson = new Person("Ad", "Min", 1987, true);
        userPerson = new Person("Non", "Min", 1987);
        secondUserPerson = new Person("Sec", "Min", 1987);
    }

    @AfterEach
    void tearDown() {
        adminPerson = null;
        userPerson = null;
        secondUserPerson = null;
    }

    @Test
    void withAdminUUID_thenCreationWorks() {
        adminPerson.setAdmin(true);
        personRepository.save(adminPerson);

        webTestClient.post()
                .uri("/person")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(userPerson)
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void withUserUUID_thenCreationFails() {
        personRepository.save(adminPerson);
        personRepository.save(userPerson);

        webTestClient.post()
                .uri("/person")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .bodyValue(secondUserPerson)
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void withNoUUID_InHeaderAndNoUsersExist_ThenCreationWorks() {
        webTestClient.post()
                .uri("/person")
                .header("secret", "hemlig123hemlig")
                .bodyValue(adminPerson)
                .exchange()
                .expectStatus().isOk();
    }


    @Test
    void withNoUUID_InHeaderAndUsersExist_ThenCreationFails() {
        personRepository.save(adminPerson);

        webTestClient.post()
                .uri("/person")
                .header("secret", "hemlig123hemlig")
                .bodyValue(userPerson)
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void withAdminUUID_ThenDeletionOfUserWorks() {
        personRepository.save(adminPerson);
        personRepository.save(userPerson);

        webTestClient.delete()
                .uri("/person")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", userPerson.getTag())
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void withUserUUID_ThenDeletionOfUserFails() {
        personRepository.save(adminPerson);
        personRepository.save(userPerson);
        personRepository.save(secondUserPerson);

        webTestClient.delete()
                .uri("/person")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .header("Target-UUID", secondUserPerson.getTag())
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    void adminPersonCannotDeleteSelf(){
        personRepository.save(adminPerson);

        webTestClient.delete()
                .uri("/person")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", adminPerson.getTag())
                .exchange()
                .expectStatus().isForbidden();
    }

}

package hig.rest;

import hig.domain.*;
import hig.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "69420")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class PaymentControllerIT {

    private Person adminPerson;
    private Person userPerson;
    private Room testRoom;
    private Cleaning testCleaning;
    private Salary testSalary;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private CleaningRepository cleaningRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private SalaryRepository salaryRepository;


    @AfterEach
    void tearDown() {
        adminPerson = null;
        userPerson = null;
        testRoom = null;
        testCleaning = null;
        testSalary = null;
    }

    @BeforeEach
    void setUp() {
        adminPerson = new Person("Ad", "Min", 1987, true);
        userPerson = new Person("Non", "Min", 1987);
        testRoom = new Room("Kitchen");
        testSalary = new Salary(LocalDate.now(), userPerson, 10L);
        testCleaning = new Cleaning(adminPerson, testRoom);

        adminPerson = personRepository.save(adminPerson);
        userPerson = personRepository.save(userPerson);
        testRoom = roomRepository.save(testRoom);
        testCleaning = cleaningRepository.save(testCleaning);
        testSalary = salaryRepository.save(testSalary);
    }

    private void createCleaningsInRepo() {
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2000, 2, 4, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2001, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2002, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2003, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2004, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2005, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2006, 2, 2, 0, 0, 0)));
    }

    @Test
    void withNoCallerUUID_creationFails() {
        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Payment-Date", LocalDate.now().toString())
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void withUserUUIDAndTargetID_creationSucceeds() {
        long expectedValue = 1;

        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Payment-Date", LocalDate.now().toString())
                .exchange()
                .expectStatus().isOk();

        assertEquals(expectedValue, paymentRepository.count());
    }

    @Test
    void withWrongPaymentDate_creationFails() {
        long expectedValue = 1;

        paymentRepository.save(new Payment(LocalDate.of(1987,12,25), userPerson, 200000L));

        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Payment-Date", LocalDate.of(1945,9,2).toString())
                .exchange()
                .expectStatus().isForbidden();
        assertEquals(expectedValue, paymentRepository.count());
    }

    @Test
    void withCorrectValues_creationSuceeds() {
        long expectedValue = 2;

        paymentRepository.save(new Payment(LocalDate.of(1987,12,25), userPerson, 200000L));

        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Payment-Date", LocalDate.of(1995,9,2).toString())
                .exchange()
                .expectStatus().isOk();
        assertEquals(expectedValue, paymentRepository.count());
    }

    //Change error type manually to badRequest later
    @Test
    void withNoTargetUUID_creationFails() {
        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .exchange()
                .expectStatus().isBadRequest();
    }


    @Test
    void withThreeCleaningsPaymentAmountCalculatedCorrectly_creationSucceeds() {
        long expectedValue = 30L;

        createCleaningsInRepo();

        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Payment-Date", LocalDate.of(2002, 3, 3).toString())
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.paymentAmount").isEqualTo(expectedValue);
    }

    @Test
    void withAllCleaningsPaymentAmountCalculatedCorrectly_creationSucceeds() {
        long expectedValue = 70L;

        createCleaningsInRepo();

        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Payment-Date", LocalDate.now().toString())
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.paymentAmount").isEqualTo(expectedValue);
    }

    @Test
    void withPaymentBeforeCleanings_PaymentAmountCalculatedSucceeds() {
        long expectedValue = 0L;

        createCleaningsInRepo();

        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Payment-Date", LocalDate.of(0, 2, 2).toString())
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.paymentAmount").isEqualTo(expectedValue);
    }

    @Test
    void salaryGetsUsedAfterPayment(){
        assertFalse(testSalary.isUsed());

        createCleaningsInRepo();

        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Payment-Date", LocalDate.of(0, 2, 2).toString())
                .exchange()
                .expectStatus().isOk();

        assertTrue(salaryRepository.getLatestSalaryByPersonID(userPerson.getId()).get().isUsed());
    }

    @Test
    void cannotChangeUsedSalary(){
        createCleaningsInRepo();

        webTestClient.put()
                .uri("/salary")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Salary-Amount", "1234")
                .exchange()
                .expectStatus().isOk();

        webTestClient.post()
                .uri("/payment")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Payment-Date", LocalDate.of(2002, 3, 3).toString())
                .exchange()
                .expectStatus().isOk();

        webTestClient.put()
                .uri("/salary")
                .header("secret", "hemlig123hemlig")
                .header("Target-UUID", userPerson.getTag())
                .header("Caller-UUID", adminPerson.getTag())
                .header("Salary-Amount", "4321")
                .exchange()
                .expectStatus().isForbidden();
    }

}

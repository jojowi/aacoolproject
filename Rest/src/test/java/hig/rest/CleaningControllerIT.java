package hig.rest;

import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import hig.service.person.PersonService;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "69420")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class CleaningControllerIT {

    private Person adminPerson;
    private Person userPerson;
    private Room testRoom;
    private Cleaning testCleaning;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private CleaningRepository cleaningRepository;

    @AfterEach
    void tearDown() {
        adminPerson = null;
        userPerson = null;
        testRoom = null;
        testCleaning = null;
    }

    @BeforeEach
    void setUp() {
        adminPerson = new Person("Ad", "Min", 1987, true);
        userPerson = new Person("Non", "Min", 1987);
        testRoom = new Room("Kitchen");
        adminPerson = personRepository.save(adminPerson);
        userPerson = personRepository.save(userPerson);
        testRoom = roomRepository.save(testRoom);
        testCleaning = new Cleaning(adminPerson, testRoom);
        testCleaning = cleaningRepository.save(testCleaning);
    }

    private void createCleaningsInRepo() {
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2023, 2, 4, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2024, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2025, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2026, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2027, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2028, 2, 2, 0, 0, 0)));
        cleaningRepository.save(new Cleaning(userPerson, testRoom, LocalDateTime.of(2029, 2, 2, 0, 0, 0)));
    }

    @Test
    void withNoCallerUUID_creationFails() {
        long expectedValue = 1;

        webTestClient.post()
                .uri("/cleaning")
                .header("secret", "hemlig123hemlig")
                .header("Room-ID", "1")
                .exchange()
                .expectStatus().isBadRequest();

        assertEquals(expectedValue, cleaningRepository.count());
    }

    @Test
    void withUserUUIDAndRoomID_creationSucceeds() {
        long expectedValue = 2;

        webTestClient.post()
                .uri("/cleaning")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .header("Room-ID", "1")
                .exchange()
                .expectStatus().isOk();

        assertEquals(expectedValue, cleaningRepository.count());
    }

    @Test
    void withAdminUUID_creationSucceeds() {
        long expectedValue = 2;

        webTestClient.post()
                .uri("/cleaning")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Room-ID", "1")
                .exchange()
                .expectStatus().isOk();

        assertEquals(expectedValue, cleaningRepository.count());
    }

    //Change error type manually to badRequest later
    @Test
    void withWrongRoomID_creationFails() {
        webTestClient.post()
                .uri("/cleaning")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Room-ID", "69420")
                .exchange()
                .expectStatus().isNotFound();
    }

    //Change error type manually to badRequest later
    //with noRoomId in url creation fails
    @Test
    void withNoRoomId_creationFails() {
        webTestClient.post()
                .uri("/cleaning")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .exchange()
                .expectStatus().isBadRequest();
    }



    void withEmptyFromDateHeaderAndRoomIdHeader_thenRetrievalFails() {
        webTestClient.get().uri("/cleaning/byroomanddate")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .exchange()
                .expectStatus().isBadRequest();
    }

    @Test
    void withCorrectRoomIdAndFromCurrentDate_thenRetrievalSucceeds() {
        cleaningRepository.save(new Cleaning(adminPerson, testRoom));

        webTestClient.get().uri("/cleaning/byroomanddate")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Room-ID", "1")
                .header("From-Date", LocalDate.now().toString())
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void withIncorrectRoomIdAndFromCurrentDate_thenRetrievalFails() {
        cleaningRepository.save(new Cleaning(adminPerson, testRoom));

        webTestClient.get().uri("/cleaning/byroomanddate")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Room-ID", "-1")
                .header("From-Date", LocalDate.now().toString())
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void withCorrectRoomIdAndNoDate_thenRetrievalSucceeds() {
        cleaningRepository.save(new Cleaning(adminPerson, testRoom));

        webTestClient.get().uri("/cleaning/byroomanddate")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Room-ID", "1")
                .header("From-Date", "")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void withIncorrectRoomIdAndNoDate_thenRetrievalFails() {
        cleaningRepository.save(new Cleaning(adminPerson, testRoom));

        webTestClient.get().uri("/cleaning/byroomanddate")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Room-ID", "-1")
                .header("From-Date", "")
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void withFromDateAndToDate_thenRetrievalSucceeds() {
        createCleaningsInRepo();

        webTestClient.get().uri("/cleaning/adminGet")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", userPerson.getTag())
                .header("From-Date", "2024-05-14")
                .header("To-Date", "2028-02-03")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(4);
    }

    @Test
    void withoutFromDateAndWithToDate_thenRetrievalSucceeds() {
        createCleaningsInRepo();
        webTestClient.get().uri("/cleaning/adminGet")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", userPerson.getTag())
                .header("From-Date", "")
                .header("To-Date", "2025-05-14")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(3);

    }

    @Test
    void withFromDateAndWithoutToDate_thenRetrievalSucceeds() {
        createCleaningsInRepo();
        webTestClient.get().uri("/cleaning/adminGet")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", userPerson.getTag())
                .header("From-Date", "2025-05-14")
                .header("To-Date", "")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(4);

    }

    @Test
    void withoutDateHeaders_thenRetrievalSucceeds() {
        createCleaningsInRepo();
        webTestClient.get().uri("/cleaning/adminGet")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", adminPerson.getTag())
                .header("Target-UUID", userPerson.getTag())
                .header("From-Date", "")
                .header("To-Date", "")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(7);
    }
    
     @Test
    void withFromDateAndToDateAsUser_thenRetrievalSucceeds() {
        createCleaningsInRepo();

        webTestClient.get().uri("/cleaning/get")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .header("From-Date", "2024-05-14")
                .header("To-Date", "2028-02-03")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(4);
    }

    @Test
    void withoutFromDateAndWithToDateAsUser_thenRetrievalSucceeds() {
        createCleaningsInRepo();
        webTestClient.get().uri("/cleaning/get")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .header("From-Date", "")
                .header("To-Date", "2025-05-14")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(3);

    }

    @Test
    void withFromDateAndWithoutToDateAsUser_thenRetrievalSucceeds() {
        createCleaningsInRepo();
        webTestClient.get().uri("/cleaning/get")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .header("From-Date", "2025-05-14")
                .header("To-Date", "")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(4);

    }

    @Test
    void withoutDateHeadersAsUser_thenRetrievalSucceeds() {
        createCleaningsInRepo();
        webTestClient.get().uri("/cleaning/get")
                .header("secret", "hemlig123hemlig")
                .header("Caller-UUID", userPerson.getTag())
                .header("From-Date", "")
                .header("To-Date", "")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(7);
    }

}

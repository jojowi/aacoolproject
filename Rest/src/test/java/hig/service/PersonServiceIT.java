package hig.service;

import hig.domain.Person;
import hig.service.person.PersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class PersonServiceIT {

    @Autowired
    PersonService personService;

    @Test
    public void givenImplicitNonAdmin_whenSavedInEmptyDatabase_thenAssignAdmin() {
        Person person = new Person("Pelle", "Karlsson", 1987);
        assertFalse(person.isAdmin());
        personService.create(person);
        assertTrue(personService.getPerson(1L).admin());
    }

    @Test
    public void givenExplicitNonAdmin_whenSavedInEmptyDatabase_thenAssignAdmin() {
        Person person = new Person("Pelle", "Karlsson", 1987, false);
        assertFalse(person.isAdmin());
        personService.create(person);
        assertTrue(personService.getPerson(1L).admin());
    }

    @Test
    public void givenExplicitAdmin_whenSavedInEmptyDatabase_thenAdminStays() {
        Person person = new Person("Pelle", "Karlsson", 1987, true);
        assertTrue(person.isAdmin());
        personService.create(person);
        assertTrue(personService.getPerson(1L).admin());
    }

}

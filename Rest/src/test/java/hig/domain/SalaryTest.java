package hig.domain;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SalaryTest {

    private Validator validator;
    private Person testPerson;

    @BeforeEach
    public void setUp() {
        testPerson = new Person("Bob", "Bobsson", 1987);
        validator = Validation.buildDefaultValidatorFactory().getValidator();

    }

    @AfterEach
    public void tearDown() {
        testPerson = null;
    }

    @Test
    void cannotGiveLessThan10KrInCompensation(){
        Salary salary = new Salary(LocalDate.now(), testPerson, 9L);
        Set<ConstraintViolation<Salary>> violations = validator.validate(salary);
        assertEquals(1, violations.size());
    }

}

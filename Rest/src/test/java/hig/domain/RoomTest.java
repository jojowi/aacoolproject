package hig.domain;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class RoomTest {


    private Validator validator;

    @BeforeEach
    void setUp() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    public void givenEmptyName_thenThenFailValidation(){
        Room room = new Room("");
        Set<ConstraintViolation<Room>> violations = validator.validate(room);
        assertEquals(1, violations.size());
    }

    @Test
    public void givenValidName_thenThenSuccessValidation(){
        Room room = new Room("Gamingröom");
        Set<ConstraintViolation<Room>> violations = validator.validate(room);
        assertEquals(0, violations.size());

    }
}
package hig.domain;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;

import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 21meno01
 */
public class PersonTest {

    private Person personUnderTest;
    private Validator validator;

    @BeforeEach
    public void setUp() {
        personUnderTest = new Person("Bob", "Bobsson", 1987);
        validator = Validation.buildDefaultValidatorFactory().getValidator();

    }

    @AfterEach
    public void tearDown() {
        personUnderTest = null;
    }

    @Test
    public void givenInvalidAge_whenSetAge_thenThrowException() {
        assertThrows(NumberFormatException.class, () -> personUnderTest.setBirthDate(106));
    }

    @Test
    public void givenValidAge_whenSetAge() {
        assertDoesNotThrow(() -> personUnderTest.setBirthDate(1999));
    }

    //Reference
    //https://www.baeldung.com/java-bean-validation-not-null-empty-blank
    @Test
    public void adminTrueValid_validationTest() {
        Person person = new Person("Peter", "Henriksson", 1999, true);
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        assertEquals(0, violations.size());
    }

    //A-ZÅÄÖ
    @Test
    public void validFirstName() {
        Person person = new Person("Örjan", "Svensson", 1999);
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        assertEquals(violations.size(), 0);
    }

    //A-ZÅÄÖ
    @Test
    public void invalidFirstName() {
        Person person = new Person("Örjan1555", "Svensson", 1999);
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        assertEquals(violations.size(), 1);
    }


    //A-ZÅÄÖ
    @Test
    public void validLastName() {
        Person person = new Person("Pelle", "örn", 1999);
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        assertEquals(violations.size(), 0);
    }

    //A-ZÅÄÖ
    @Test
    public void invalidLastName() {
        Person person = new Person("Pelle", "Örn245", 1999);
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        assertEquals(violations.size(), 1);
    }

}

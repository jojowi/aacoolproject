
package hig.rest;

import hig.annotation.AdminOnly;
import hig.annotation.TargetRequired;
import hig.service.person.PersonService;
import hig.domain.Person;
import hig.service.person.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("person")
public class PersonController {

    private final PersonService service;

    @Autowired
    public PersonController(PersonService service) {
        this.service = service;
    }

    @GetMapping
    public List<PersonSimpleDTO> findAll() {
        return service.getAllPeople();
    }

    @GetMapping("{id}")
    public PersonSimpleDTO findOne(@PathVariable Long id) {
        return service.getPerson(id);
    }

    @AdminOnly
    @PostMapping
    public PersonSimpleDTO createNew(@RequestBody Person person) {
        return service.create(person);
    }

    @AdminOnly
    @TargetRequired
    @DeleteMapping
    public Integer deleteByTag(@RequestHeader("Target-UUID") String targetUUID){
        return service.deletePersonByTag(targetUUID);
    }
}

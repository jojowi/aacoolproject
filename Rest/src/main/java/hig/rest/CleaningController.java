package hig.rest;

import hig.annotation.AdminOnly;
import hig.annotation.RoomRequired;
import hig.annotation.TargetRequired;
import hig.repository.CleaningRepository;
import hig.service.cleaning.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author thomas
 */
@RestController
@RequestMapping("cleaning")
public class CleaningController {

    private final CleaningService service;

    public CleaningController(CleaningService service) {
        this.service = service;
    }

    @PostMapping
    public CleaningDTO create(@RequestHeader("Caller-UUID") String callerUUID, @RequestHeader("Room-ID") Long roomId) {
        return service.create(roomId, callerUUID);
    }

    @AdminOnly
    @GetMapping
    public List<CleaningDTO> getAll(@RequestHeader("Caller-UUID") String callerUUID) {
        return service.getAllCleanings();
    }

    @RoomRequired
    @GetMapping("byroomanddate")
    public List<CleaningDTO> getCleaningsByRoomIdAndDate(
            @RequestHeader("Caller-UUID") String callerUUID,
            @RequestHeader("Room-ID") Long roomId,
            @RequestHeader(value = "From-Date", required = false) String fromDateHeader) {

        LocalDate date = (fromDateHeader == null || fromDateHeader.isBlank()) ? LocalDate.now() : LocalDate.parse(fromDateHeader, DateTimeFormatter.ISO_DATE);

        return service.getCleaningsByRoomIdAndDate(roomId, date.atStartOfDay());
    }

    @GetMapping("{daysFromNow}")
    public List<CleaningDTO> getByPersonAndDaysForward(@RequestHeader("Caller-UUID") String callerUUID, @PathVariable Long daysFromNow){
        return service.getByPersonIdAndDaysForward(callerUUID, daysFromNow);
    }
    
    
    @GetMapping("get")
    public List<CleaningDTO> getCleaningsByUserTag(
            @RequestHeader("Caller-UUID") String callerUUID,
            @RequestHeader(value = "From-Date", required = false) String fromDateHeader,
            @RequestHeader(value = "To-Date", required = false) String toDateHeader
    ) {
        LocalDate fromDate = (fromDateHeader == null || fromDateHeader.isBlank()) ? LocalDate.of(1,1,1) : LocalDate.parse(fromDateHeader, DateTimeFormatter.ISO_DATE);
        LocalDate toDate = (toDateHeader == null || toDateHeader.isBlank()) ? LocalDate.of(9999,12,31): LocalDate.parse(toDateHeader, DateTimeFormatter.ISO_DATE);

        return service.getCleaningsByUserTag(callerUUID, fromDate.atStartOfDay(), toDate.atTime(23, 59, 59));
    }

    

    @TargetRequired
    @AdminOnly
    @GetMapping("adminGet")
    public List<CleaningDTO> getCleaningsByUserTag(
            @RequestHeader("Caller-UUID") String callerUUID,
            @RequestHeader("Target-UUID") String targetUUID,
            @RequestHeader(value = "From-Date", required = false) String fromDateHeader,
            @RequestHeader(value = "To-Date", required = false) String toDateHeader
    ) {
        LocalDate fromDate = (fromDateHeader == null || fromDateHeader.isBlank()) ? LocalDate.of(1,1,1) : LocalDate.parse(fromDateHeader, DateTimeFormatter.ISO_DATE);
        LocalDate toDate = (toDateHeader == null || toDateHeader.isBlank()) ? LocalDate.of(9999,12,31): LocalDate.parse(toDateHeader, DateTimeFormatter.ISO_DATE);

        return service.getCleaningsByUserTag(targetUUID, fromDate.atStartOfDay(), toDate.atTime(23, 59, 59));
    }

}

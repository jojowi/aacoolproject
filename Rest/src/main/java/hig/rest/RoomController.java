package hig.rest;

import hig.annotation.AdminOnly;
import hig.domain.Room;
import hig.service.room.*;

import java.util.List;

import org.springframework.web.bind.annotation.*;

/**
 * @author thomas
 */
@RestController
@RequestMapping("room")
public class RoomController {

    private final RoomService service;
    private final RoomMapper mapper;

    public RoomController(
            RoomService service,
            RoomMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @AdminOnly
    @PostMapping
    public RoomDTO create(@RequestBody Room room) {
        return mapper.toDto(service.create(room));
    }

    @AdminOnly
    @DeleteMapping
    public String deleteByRoomID(@RequestHeader("Room-ID") String roomID) {
        return "Number of deleted rows: " + service.deleteByRoomID(roomID);
    }

    @GetMapping
    public List<RoomDTO> findAll() {
        return service.findAll();
    }
}

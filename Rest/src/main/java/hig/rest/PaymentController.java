package hig.rest;

import hig.annotation.AdminOnly;
import hig.annotation.TargetRequired;
import hig.service.payment.PaymentDTO;
import hig.service.payment.PaymentMapper;
import hig.service.payment.PaymentService;
import hig.service.salary.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("payment")
public class PaymentController {

    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
 
    }
    
    @GetMapping
    public List<PaymentDTO> findAll(){ return paymentService.findAll(); }

    @AdminOnly
    @PostMapping
    @TargetRequired
    public PaymentDTO create(@RequestHeader("Caller-UUID") String callerUUID,
                            @RequestHeader("Target-UUID") String targetUUID,
                            @RequestHeader("Payment-Date") LocalDate paymentDate) {
        return paymentService.create(targetUUID, paymentDate);
    }
}

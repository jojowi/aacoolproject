package hig.rest;

import hig.annotation.AdminOnly;
import hig.annotation.TargetRequired;
import hig.service.salary.SalaryDTO;
import hig.service.salary.SalaryService;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("salary")
public class SalaryController {

    private final SalaryService service;

    public SalaryController(SalaryService service) {
        this.service = service;
    }

    @AdminOnly
    @GetMapping
    public List<SalaryDTO> getAll(@RequestHeader("Caller-UUID") String callerUUID) {
        return service.getAllSalaries();
    }

    @AdminOnly
    @PostMapping
    @TargetRequired
    public SalaryDTO create(@RequestHeader("Caller-UUID") String callerUUID,
                            @RequestHeader("Target-UUID") String targetUUID,
                            @RequestHeader("Effective-Date") LocalDate effectiveDate,
                            @RequestHeader("Salary-Amount") Long salaryAmount) {
        return service.create(targetUUID, effectiveDate, salaryAmount);
    }

    @AdminOnly
    @PutMapping
    @TargetRequired
    public void update(@RequestHeader("Caller-UUID") String callerUUID,
                            @RequestHeader("Target-UUID") String targetUUID,
                            @RequestHeader("Salary-Amount") Long salaryAmount) {
        service.update(targetUUID, salaryAmount);
    }
}

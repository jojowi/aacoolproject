package hig.exception;

import java.time.DateTimeException;
import java.util.NoSuchElementException;

import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.ConstraintViolationException;
import org.apache.coyote.BadRequestException;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception handler for REST controllers.
 *
 * @author thomas
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({
            SecurityException.class,
            NoSuchElementException.class,
            EntityNotFoundException.class,
            ConstraintViolationException.class,
            DateTimeException.class,
    })
    public ResponseEntity<ExceptionWrapper> handleException(Exception ex) {
        HttpStatus status = switch (ex.getClass().getSimpleName()) {
            case "SecurityException", "ConstraintViolationException", "DateTimeException" -> HttpStatus.FORBIDDEN;
            case "NoSuchElementException", "EntityNotFoundException" -> HttpStatus.NOT_FOUND;
            default -> HttpStatus.INTERNAL_SERVER_ERROR; // Fallback to 500 for unexpected exceptions
        };

        return new ResponseEntity<>(new ExceptionWrapper(ex), status);
    }

    public static class ExceptionWrapper {

        private final String type;
        private final String message;

        public ExceptionWrapper(Exception ex) {
            this.type = ex.getClass().getSimpleName();
            this.message = ex.getMessage();
        }

        public String getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }
    }
}

package hig.aspect;

import hig.service.logging.*;
import java.util.stream.Stream;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Aspect
@Component
public class LoggingAspect {

    private final LogAdapter logAdapter;

    @Autowired
    public LoggingAspect(LogAdapter logAdapter) {
        this.logAdapter = logAdapter;
    }
    
    @Around("execution(* hig.service..*Service.*(..)))")
    public Object logServiceCall(ProceedingJoinPoint joinPoint) throws Throwable {
        logAdapter.log(Level.INFO, () -> joinPoint.getSignature().getName() + " called with args " + Stream.of(joinPoint.getArgs()).toList().toString());
        Object result = joinPoint.proceed();
        logAdapter.log(Level.INFO, () -> joinPoint.getSignature().getName() + " returning " + result);
        return result;
    }
}

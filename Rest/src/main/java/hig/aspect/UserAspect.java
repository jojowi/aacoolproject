package hig.aspect;

import hig.repository.PersonRepository;
import hig.service.person.PersonService;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author 22chla01
 */
@Component
@Aspect
public class UserAspect {

    @Autowired
    private HttpServletRequest request;
    private final PersonRepository personRepository;
    private final PersonService personService;

    public UserAspect(PersonRepository personRepository, PersonService personService) {
        this.personRepository = personRepository;
        this.personService = personService;
    }

    @Before("execution(* hig.rest..*Controller.*(..))")
    public void checkAuthorization() {
        checkSecret();

        String requestUUID = request.getHeader("Caller-UUID");

        if (personRepository.count() > 0) {
            Optional.ofNullable(personRepository.findByTag(requestUUID).orElseThrow(() -> new SecurityException("No user found with the provided Caller-UUID.")));
        }
    }

    private void checkSecret(){
        Optional.ofNullable(request.getHeader("secret"))
                .filter(s -> s.equals("hemlig123hemlig"))
                .orElseThrow(() -> new SecurityException("Invalid secret"));
    }

}
package hig.aspect;

import hig.repository.PersonRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Aspect
public class TargetRequiredAspect {

    @Autowired
    private HttpServletRequest request;

    private PersonRepository personRepository;

    public TargetRequiredAspect(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Before("@annotation(hig.annotation.TargetRequired)")
    public void checkTargetIsValid() {
        String callerUUID = Optional.ofNullable(request.getHeader("Caller-UUID")).orElseThrow(() -> new SecurityException("Invalid Caller-UUID"));
        String targetUUID = Optional.ofNullable(request.getHeader("Target-UUID")).orElseThrow(() -> new SecurityException("Invalid Target-UUID"));

        if (targetUUID.equals(callerUUID)) {
            throw new SecurityException("Caller-UUID and Target-UUID cannot be the same");
        }

        if (personRepository.findByTag(targetUUID).isEmpty()) {
            throw new SecurityException("Target user does not exist in database");
        }
    }

}
package hig.aspect;


import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import hig.service.person.PersonService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Aspect
public class RoomRequiredAspect {

    @Autowired
    private HttpServletRequest request;

    private final RoomRepository roomRepository;

    public RoomRequiredAspect(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Before("@annotation(hig.annotation.RoomRequired)")
    public void checkRoomExists() {
        String id = Optional.ofNullable(request.getHeader("Room-ID")).orElseThrow(() -> new EntityNotFoundException("Invalid Room-ID"));
        if (!roomRepository.existsById(Long.valueOf(id))) {
            throw new EntityNotFoundException("Room does not exist");
        }
    }

}

package hig.aspect;

import hig.domain.Person;
import hig.repository.PersonRepository;
import hig.service.person.PersonService;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Aspect
@Component
public class AdminOnlyAspect {

    @Autowired
    private HttpServletRequest request;
    private final PersonRepository personRepository;
    private final PersonService personService;

    public AdminOnlyAspect(PersonRepository personRepository, PersonService personService) {
        this.personRepository = personRepository;
        this.personService = personService;
    }

    @Before("@annotation(hig.annotation.AdminOnly)")
    public void checkAdminAuthorization() {
        String callerUUID = request.getHeader("Caller-UUID");

        if (isInvalidUUID(callerUUID)) {
            throw new SecurityException("Invalid Caller-UUID");
        }

        if (isNotAdmin(callerUUID)) {
            throw new SecurityException("Admin permissions required");
        }
    }

    private boolean isInvalidUUID(String requestUUID) {
        return requestUUID == null && personRepository.count() > 0;
    }

    private boolean isNotAdmin(String requestUUID) {
        if (personRepository.count() == 0) {
            return false;
        }

        Optional<Person> person = personRepository.findByTag(requestUUID);

        return person.isEmpty() || !person.get().isAdmin();
    }
}
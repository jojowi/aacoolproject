package hig.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;

import java.time.LocalDateTime;

/**
 * @author thomas
 */
@Entity
public class Cleaning {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime dateTimeStamp = LocalDateTime.now();

    @ManyToOne
    private Room room;

    @ManyToOne
    private Person person;

    protected Cleaning() {
    }

    public Cleaning(Person person, Room room) {
        this.room = room;
        this.person = person;
    }

    public Cleaning(Person person, Room room, LocalDateTime dateTimeStamp) {
        this.room = room;
        this.person = person;
        this.dateTimeStamp = dateTimeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTimeStamp() {
        return dateTimeStamp;
    }

    public void setDateTimeStamp(LocalDateTime time) {
        this.dateTimeStamp = time;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}

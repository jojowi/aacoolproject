package hig.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;

import java.time.LocalDate;

@Entity
public class Salary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate effectiveDate;

    @Min(10)
    private Long salaryAmount;

    @ManyToOne
    private Person person;
    private boolean used = false;

    public Salary() {

    }

    public Salary(LocalDate effectiveDate, Person person, Long salaryAmount) {
        this.effectiveDate = effectiveDate;
        this.person = person;
        this.salaryAmount = salaryAmount;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public Long getSalaryAmount() {
        return salaryAmount;
    }

    public void setSalaryAmount(Long payment) {
        this.salaryAmount = payment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}

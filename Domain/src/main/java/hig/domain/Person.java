package hig.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.time.Year;
import java.util.UUID;
import java.util.Objects;

/**
 *
 * @author thomas
 */
@Entity
public class Person {

    private static final int MAX_AGE = 120;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Pattern(regexp = "[A-Za-zÅÄÖåäö]+", message = "firstName contains invalid characters")
    private String firstName;


    @Pattern(regexp = "[A-Za-zÅÄÖåäö]+", message = "lastName contains invalid characters")
    private String lastName;

    private Boolean admin = false;

    @NotNull
    private Integer birthDate;

    @Column(unique = true)
    private String tag = UUID.randomUUID().toString();

    public Person(String firstName, String lastName, Integer birthDate) {
        this(firstName, lastName, birthDate, false);
    }

    public Person(String firstName, String lastName, Integer birthDate, Boolean admin) {
        setFirstName(firstName);
        setLastName(lastName);
        setBirthDate(birthDate);
        setAdmin(admin);
    }

    public Person() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;

    }

    public Integer getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Integer birthDate) {
        if (birthDate < Year.now().getValue() - MAX_AGE || birthDate >= Year.now().getValue()) {
            throw new NumberFormatException("Invalid birthdate");
        }
        this.birthDate = birthDate;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
    @Override
    public String toString(){
        return firstName + " " + lastName + " " + birthDate;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) &&
               Objects.equals(lastName, person.lastName) &&
               Objects.equals(birthDate, person.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, birthDate);
    }
}

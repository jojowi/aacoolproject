package hig.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;

/**
 *
 * @author thomas
 */
@Entity
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Pattern(regexp = "[A-Za-zÅÄÖåäö]+", message = "Room name contains invalid characters")
    private String name;

    protected Room() {
    }

    public Room(String name) {
        this.name = name;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
